package netflix

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Log)
class LogSpec extends Specification {

    def fileFullPath
    
    def setup() {
        fileFullPath = System.properties['base.dir'] + File.separator + "test-log.txt"        
    }

    def cleanup() {
    }

    void testAnalysis() {
        log.info "start testing"
        Log logger = new Log(fileFullPath)
        assert 5 == logger.getnumOfUniqueIPs()        
        assert 3 == logger.getnumOfUniqueHTTPCodes()
    }
}
