<html>
    <head><metaname="layout" content="main"/>
        <link rel="stylesheet" href="../css/main.css" type="text/css" />
    </head>
    <body>
        <div id="grailsLogo" role="banner">
            <a href="http://grails.org">
                <img src="/netflix/static/images/grails_logo.png" alt="Grails">
            </a>
        </div>
        <div id="page-body">
            <br><br>
            <h1>The Log could not be processed:<br>
            please make sure to upload a log in the format of Apache access-log</h1>
            <table>
                <tr>
                    <td colspan="2">
                        <a style="text-decoration: none" href='/netflix/upload'><b>Upload another log file</b> &nbsp;<img src='../images/upload.png'></a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="footer" role="contentinfo"></div>
    </body>
</html>
