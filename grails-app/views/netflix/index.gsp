<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <metaname="layout" content="main"/>
        <link rel="stylesheet" href="../css/main.css" type="text/css" />
    </head>
    <body>
        <div id="grailsLogo" role="banner"><a href="http://grails.org">
                <img src="/netflix/static/images/grails_logo.png" alt="Grails"></a>
        </div>
        <div id="page-body"><br><br>
            <h1><b>Log Analysis Report</b></h1>
            <table><tr><td><h4>Number of unique IPs: ${log.getNumOfUniqueIPs()}</h4><td></tr>
                    <tr><td><h4>Number of unique HTTP codes: ${log.getNumOfUniqueHTTPCodes()}</h4></td></tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td><h4>Number of occurrences for each HTTP code:</h4></td></tr>
                    <table border="1"><tr><th>IP</th><th># Occ</th></tr>
                        <g:each in="${log.httpCodes}" var="item">
                           <tr><td>${item.key?.encodeAsHTML()}</td><td>${item.value?.encodeAsHTML()}</td></tr>
                        </g:each>
                    </table>
            </table>
            <table>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td><h4>Number of occurrences for each IP:</h4></td></tr>
                    <table border="1"><tr><th>IP</th><th># Occ</th></tr>
                        <g:each in="${log.ips}" var="item">
                           <tr><td>${item.key?.encodeAsHTML()}</td><td>${item.value?.encodeAsHTML()}</td></tr>
                        </g:each>
                    </table>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
        
            </table>
            <table><tr><td colspan="2"><a style="text-decoration: none" href='/netflix/upload'><b>Upload another log file</b> &nbsp;<img src='../images/upload.png'></a></td></tr></table>
        </div>
        <div class="footer" role="contentinfo"></div>
    </body>
</html>

