package netflix

import grails.converters.*

class RestController {
    
    def index() {        
        def output = generateOutput()
        response.status = 200 // OK
        // format properly
        withFormat {
            xml {
                render output as XML
            }
            json {
                render output as JSON
            }
        }
    }
    
    // grab log file, create the analyzed log object and format the response
    private generateOutput(){
        def fileFullPath = System.properties['base.dir'] + File.separator + "logs" + File.separator + "log.txt"
        def log = LogAnalyzerRESTService.runAnalysis(fileFullPath)        
        // prepare response 
        def output = [:]
        output['numOfUniqueIPs'] = log.getNumOfUniqueIPs()
        output['numOfUniqueHTTPCodes'] = log.getNumOfUniqueHTTPCodes()
        def ips = log.getips() 
        output['ips'] = [ips]
        def httpCodes = log.gethttpCodes() 
        output['httpCodes'] = httpCodes
        return output
    }
}
