package netflix

class UploadController {
    
    def index() {
        
        if(params.uploadFile){
            if(params.uploadFile instanceof org.springframework.web.multipart.commons.CommonsMultipartFile){
                def fileFullPath = System.properties['base.dir'] + File.separator + "logs" + File.separator + "log.txt"
                new FileOutputStream(fileFullPath).leftShift( params.uploadFile.getInputStream() );
            }else{
                log.error("wrong attachment type [${uploadFile.getClass()}]");
            }
            redirect(controller:"netflix")
        }
    }
}
