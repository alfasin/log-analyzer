package netflix

class NetflixController {

    def index() { 
        def fileFullPath = System.properties['base.dir'] + File.separator + "logs" + File.separator + "log.txt"
        def display = LogAnalyzerService.runAnalysis(fileFullPath)
        if("process_error".equals(display)){
            render(view:display)
            return
        }
        [log:display]
    }
}
