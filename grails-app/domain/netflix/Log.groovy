package netflix
/*
 * author: alfasin
 * 11/12/2013
 * 
 * LogAnalyzer takes as an input a log-file in the following format:

95.108.150.235 - - [19/Mar/2012:06:04:40 -0700] "GET / HTTP/1.1" 200 3681 "-" "Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"
69.53.237.65 - - [19/Mar/2012:11:01:16 -0700] "GET / HTTP/1.1" 200 3684 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:10.0.2) Gecko/20100101 Firefox/10.0.2"
69.53.237.65 - - [19/Mar/2012:11:01:19 -0700] "GET /favicon.ico HTTP/1.1" 200 275 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:10.0.2) Gecko/20100101 Firefox/10.0.2"
123.125.71.85 - - [19/Mar/2012:12:01:53 -0700] "GET /robots.txt HTTP/1.1" 200 454 "-" "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)"
180.76.5.173 - - [19/Mar/2012:16:53:19 -0700] "GET / HTTP/1.1" 200 3647 "-" "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)"
78.137.161.186 - - [19/Mar/2012:22:56:17 -0700] "GET /muieblackcat HTTP/1.1" 404 881 "" ""

and preforms The following analysis:
1. counts the number of unique IP address
2. counts the number of requests sent from each IP
3. counts unique HTTP response codes
4. counts the number of appearences of each unique HTTP response code

 * 
 */

class Log {
    
    /* Params */
    
    // count the number of appearences of each IP in the log
    def ips = [:]
    
    int numOfUniqueIPs = 0
    
    // count the number of appearences of each HTTP ccode in the log
    def httpCodes = [:]
    
    int numOfUniqueHTTPCodes = 0
    
    /* getters */
    public Map getips(){
        return ips 
    }

    public int getnumOfUniqueIPs(){
        return numOfUniqueIPs
    }
    
    public Map gethttpCodes(){
        return httpCodes
    }
    
    public int getnumOfUniqueHTTPCodes(){
        return numOfUniqueHTTPCodes
    }
    
    /* constructors*/
    Log(){
        def fileFullPath = System.properties['base.dir'] + File.separator + "logs" + File.separator + "log.txt"
        def log = new File(fileFullPath)
        analyzeLog(log)
    }
    
    Log(filename){
        def log = new File(filename)
        if(!log.file) return // file couldn't be loaded
        analyzeLog(log)
    }
    
    /* 
     * Helper method: 
     * go over each line of the log and grab the matched pattern 
     * this method is being used both to count unique IPs as well as HTTP response codes
     */
    private def countIntoMap(line, pattern, map){
       def matcher = (line =~ pattern)
       if(!matcher) return // in case the log is in bad format
       def item = matcher[0][0] // the captured group will be stored here

       if(map.get(item) == null){        
            map.put((item), 1)
       }
       else{        
            map.put((item), map.get(item)+1)
       }
    }

    /* 
     * Main method: go over each line in the log and count 
     * the number of unique IPs and their occurences. Do the same 
     * with HTTP response code
     */
    public def analyzeLog(log){
        // regex-pattern used to grab the IP address from the log
        def ipPattern = /^(\d+.\d+.\d+.\d+)/

        // regex-pattern used to grab the HTTP code from the log
        def httpCodePattern = / (\d{3}) /
        
        log.eachLine {
            // count the statistics for each item (IP,HTTP-code)
            countIntoMap(it, ipPattern, ips) 
            countIntoMap(it, httpCodePattern, httpCodes) 
         }
         // sort maps by number of occurrences (desc) and save size
         ips = ips.sort { -it.value }
         numOfUniqueIPs = ips.size()
         httpCodes = httpCodes.sort { -it.value }
         numOfUniqueHTTPCodes = httpCodes.size()
    }    
}
