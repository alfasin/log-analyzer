package netflix

import grails.transaction.Transactional

@Transactional
class LogAnalyzerService {    

    static def runAnalysis(filname) {
        Log log = new Log(filname)
        if(log.getNumOfUniqueHTTPCodes() == 0){
            return "process_error"
        }
        return log
    }
    
   
}
